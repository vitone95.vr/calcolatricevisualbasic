﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Title = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Result = New System.Windows.Forms.Label()
        Me.BtnAdd = New System.Windows.Forms.Button()
        Me.BtnMin = New System.Windows.Forms.Button()
        Me.BtnDiv = New System.Windows.Forms.Button()
        Me.BtnMolt = New System.Windows.Forms.Button()
        Me.txtOp1 = New System.Windows.Forms.TextBox()
        Me.txtOp2 = New System.Windows.Forms.TextBox()
        Me.lblOperator = New System.Windows.Forms.Label()
        Me.lblDisplayResult = New System.Windows.Forms.Label()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDisplay = New System.Windows.Forms.TextBox()
        Me.btnBS = New System.Windows.Forms.Button()
        Me.btnCE = New System.Windows.Forms.Button()
        Me.btnC = New System.Windows.Forms.Button()
        Me.btnPM = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btnPlus = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btnMinus = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btnCross = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btnResult = New System.Windows.Forms.Button()
        Me.btnDivide = New System.Windows.Forms.Button()
        Me.btn0 = New System.Windows.Forms.Button()
        Me.btnDot = New System.Windows.Forms.Button()
        Me.LogDisplay = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.Title.Location = New System.Drawing.Point(12, 9)
        Me.Title.Margin = New System.Windows.Forms.Padding(3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(154, 20)
        Me.Title.TabIndex = 16
        Me.Title.Text = "Calcolatrice semplice"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label1.Location = New System.Drawing.Point(281, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 19)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Operatori:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label2.Location = New System.Drawing.Point(12, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 19)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Operazione:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label3.Location = New System.Drawing.Point(12, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 19)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Operando 1:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label4.Location = New System.Drawing.Point(12, 161)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 19)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Operando 2:"
        '
        'Result
        '
        Me.Result.AutoSize = True
        Me.Result.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Result.Location = New System.Drawing.Point(12, 207)
        Me.Result.Name = "Result"
        Me.Result.Size = New System.Drawing.Size(65, 19)
        Me.Result.TabIndex = 21
        Me.Result.Text = "Risultato:"
        '
        'BtnAdd
        '
        Me.BtnAdd.Font = New System.Drawing.Font("Segoe UI", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BtnAdd.Location = New System.Drawing.Point(246, 113)
        Me.BtnAdd.Name = "BtnAdd"
        Me.BtnAdd.Size = New System.Drawing.Size(62, 38)
        Me.BtnAdd.TabIndex = 22
        Me.BtnAdd.Text = "+"
        Me.BtnAdd.UseVisualStyleBackColor = True
        '
        'BtnMin
        '
        Me.BtnMin.Font = New System.Drawing.Font("Segoe UI", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BtnMin.Location = New System.Drawing.Point(320, 113)
        Me.BtnMin.Name = "BtnMin"
        Me.BtnMin.Size = New System.Drawing.Size(64, 38)
        Me.BtnMin.TabIndex = 23
        Me.BtnMin.Text = "-"
        Me.BtnMin.UseVisualStyleBackColor = True
        '
        'BtnDiv
        '
        Me.BtnDiv.Font = New System.Drawing.Font("Segoe UI", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BtnDiv.Location = New System.Drawing.Point(320, 153)
        Me.BtnDiv.Name = "BtnDiv"
        Me.BtnDiv.Size = New System.Drawing.Size(64, 38)
        Me.BtnDiv.TabIndex = 24
        Me.BtnDiv.Text = "/"
        Me.BtnDiv.UseVisualStyleBackColor = True
        '
        'BtnMolt
        '
        Me.BtnMolt.Font = New System.Drawing.Font("Segoe UI", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BtnMolt.Location = New System.Drawing.Point(246, 153)
        Me.BtnMolt.Name = "BtnMolt"
        Me.BtnMolt.Size = New System.Drawing.Size(62, 38)
        Me.BtnMolt.TabIndex = 25
        Me.BtnMolt.Text = "*"
        Me.BtnMolt.UseVisualStyleBackColor = True
        '
        'txtOp1
        '
        Me.txtOp1.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.txtOp1.Location = New System.Drawing.Point(104, 94)
        Me.txtOp1.Name = "txtOp1"
        Me.txtOp1.Size = New System.Drawing.Size(100, 25)
        Me.txtOp1.TabIndex = 26
        '
        'txtOp2
        '
        Me.txtOp2.AcceptsReturn = True
        Me.txtOp2.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.txtOp2.Location = New System.Drawing.Point(104, 161)
        Me.txtOp2.Name = "txtOp2"
        Me.txtOp2.Size = New System.Drawing.Size(100, 25)
        Me.txtOp2.TabIndex = 27
        '
        'lblOperator
        '
        Me.lblOperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOperator.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblOperator.Location = New System.Drawing.Point(128, 128)
        Me.lblOperator.Name = "lblOperator"
        Me.lblOperator.Padding = New System.Windows.Forms.Padding(20, 0, 20, 0)
        Me.lblOperator.Size = New System.Drawing.Size(53, 23)
        Me.lblOperator.TabIndex = 29
        '
        'lblDisplayResult
        '
        Me.lblDisplayResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDisplayResult.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblDisplayResult.Location = New System.Drawing.Point(104, 206)
        Me.lblDisplayResult.Name = "lblDisplayResult"
        Me.lblDisplayResult.Padding = New System.Windows.Forms.Padding(20, 0, 20, 0)
        Me.lblDisplayResult.Size = New System.Drawing.Size(100, 23)
        Me.lblDisplayResult.TabIndex = 30
        '
        'BtnClear
        '
        Me.BtnClear.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BtnClear.Location = New System.Drawing.Point(129, 266)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(75, 23)
        Me.BtnClear.TabIndex = 31
        Me.BtnClear.Text = "Cancella"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BtnExit.Location = New System.Drawing.Point(246, 266)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(75, 23)
        Me.BtnExit.TabIndex = 32
        Me.BtnExit.Text = "Esci"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.Label5.Location = New System.Drawing.Point(12, 318)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(175, 20)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "Calcolatrice tradizionale"
        '
        'txtDisplay
        '
        Me.txtDisplay.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.txtDisplay.Location = New System.Drawing.Point(12, 380)
        Me.txtDisplay.Multiline = True
        Me.txtDisplay.Name = "txtDisplay"
        Me.txtDisplay.Size = New System.Drawing.Size(439, 49)
        Me.txtDisplay.TabIndex = 34
        Me.txtDisplay.Text = "0"
        Me.txtDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnBS
        '
        Me.btnBS.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnBS.Location = New System.Drawing.Point(12, 444)
        Me.btnBS.Name = "btnBS"
        Me.btnBS.Size = New System.Drawing.Size(104, 68)
        Me.btnBS.TabIndex = 35
        Me.btnBS.Text = "⌫"
        Me.btnBS.UseVisualStyleBackColor = True
        '
        'btnCE
        '
        Me.btnCE.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnCE.Location = New System.Drawing.Point(122, 444)
        Me.btnCE.Name = "btnCE"
        Me.btnCE.Size = New System.Drawing.Size(104, 68)
        Me.btnCE.TabIndex = 36
        Me.btnCE.Text = "CE"
        Me.btnCE.UseVisualStyleBackColor = True
        '
        'btnC
        '
        Me.btnC.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnC.Location = New System.Drawing.Point(231, 444)
        Me.btnC.Name = "btnC"
        Me.btnC.Size = New System.Drawing.Size(104, 68)
        Me.btnC.TabIndex = 38
        Me.btnC.Text = "C"
        Me.btnC.UseVisualStyleBackColor = True
        '
        'btnPM
        '
        Me.btnPM.Font = New System.Drawing.Font("Segoe UI", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnPM.Location = New System.Drawing.Point(347, 444)
        Me.btnPM.Name = "btnPM"
        Me.btnPM.Size = New System.Drawing.Size(104, 68)
        Me.btnPM.TabIndex = 37
        Me.btnPM.Text = " ± "
        Me.btnPM.UseVisualStyleBackColor = True
        '
        'btn9
        '
        Me.btn9.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn9.Location = New System.Drawing.Point(231, 532)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(104, 68)
        Me.btn9.TabIndex = 42
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = True
        '
        'btnPlus
        '
        Me.btnPlus.Font = New System.Drawing.Font("Segoe UI", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnPlus.Location = New System.Drawing.Point(347, 532)
        Me.btnPlus.Name = "btnPlus"
        Me.btnPlus.Size = New System.Drawing.Size(104, 68)
        Me.btnPlus.TabIndex = 41
        Me.btnPlus.Text = "+"
        Me.btnPlus.UseVisualStyleBackColor = True
        '
        'btn8
        '
        Me.btn8.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn8.Location = New System.Drawing.Point(122, 532)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(104, 68)
        Me.btn8.TabIndex = 40
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = True
        '
        'btn7
        '
        Me.btn7.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn7.Location = New System.Drawing.Point(12, 532)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(104, 68)
        Me.btn7.TabIndex = 39
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = True
        '
        'btn6
        '
        Me.btn6.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn6.Location = New System.Drawing.Point(231, 616)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(104, 68)
        Me.btn6.TabIndex = 46
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = True
        '
        'btnMinus
        '
        Me.btnMinus.Font = New System.Drawing.Font("Segoe UI", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnMinus.Location = New System.Drawing.Point(347, 616)
        Me.btnMinus.Name = "btnMinus"
        Me.btnMinus.Size = New System.Drawing.Size(104, 68)
        Me.btnMinus.TabIndex = 45
        Me.btnMinus.Text = "-"
        Me.btnMinus.UseVisualStyleBackColor = True
        '
        'btn5
        '
        Me.btn5.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn5.Location = New System.Drawing.Point(122, 616)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(104, 68)
        Me.btn5.TabIndex = 44
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn4.Location = New System.Drawing.Point(12, 616)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(104, 68)
        Me.btn4.TabIndex = 43
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn3.Location = New System.Drawing.Point(231, 701)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(104, 68)
        Me.btn3.TabIndex = 50
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btnCross
        '
        Me.btnCross.Font = New System.Drawing.Font("Segoe UI", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnCross.Location = New System.Drawing.Point(347, 701)
        Me.btnCross.Name = "btnCross"
        Me.btnCross.Size = New System.Drawing.Size(104, 68)
        Me.btnCross.TabIndex = 49
        Me.btnCross.Text = "*"
        Me.btnCross.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn2.Location = New System.Drawing.Point(122, 701)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(104, 68)
        Me.btn2.TabIndex = 48
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn1.Location = New System.Drawing.Point(12, 701)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(104, 68)
        Me.btn1.TabIndex = 47
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'btnResult
        '
        Me.btnResult.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnResult.Location = New System.Drawing.Point(231, 792)
        Me.btnResult.Name = "btnResult"
        Me.btnResult.Size = New System.Drawing.Size(104, 68)
        Me.btnResult.TabIndex = 54
        Me.btnResult.Text = "="
        Me.btnResult.UseVisualStyleBackColor = True
        '
        'btnDivide
        '
        Me.btnDivide.Font = New System.Drawing.Font("Segoe UI", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnDivide.Location = New System.Drawing.Point(347, 792)
        Me.btnDivide.Name = "btnDivide"
        Me.btnDivide.Size = New System.Drawing.Size(104, 68)
        Me.btnDivide.TabIndex = 53
        Me.btnDivide.Text = "/"
        Me.btnDivide.UseVisualStyleBackColor = True
        '
        'btn0
        '
        Me.btn0.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btn0.Location = New System.Drawing.Point(122, 792)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(104, 68)
        Me.btn0.TabIndex = 52
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = True
        '
        'btnDot
        '
        Me.btnDot.Font = New System.Drawing.Font("Segoe UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.btnDot.Location = New System.Drawing.Point(12, 792)
        Me.btnDot.Name = "btnDot"
        Me.btnDot.Size = New System.Drawing.Size(104, 68)
        Me.btnDot.TabIndex = 51
        Me.btnDot.Text = ","
        Me.btnDot.UseVisualStyleBackColor = True
        '
        'LogDisplay
        '
        Me.LogDisplay.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.LogDisplay.Font = New System.Drawing.Font("Segoe UI", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.LogDisplay.Location = New System.Drawing.Point(12, 341)
        Me.LogDisplay.Name = "LogDisplay"
        Me.LogDisplay.Size = New System.Drawing.Size(439, 36)
        Me.LogDisplay.TabIndex = 55
        Me.LogDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 881)
        Me.Controls.Add(Me.LogDisplay)
        Me.Controls.Add(Me.btnResult)
        Me.Controls.Add(Me.btnDivide)
        Me.Controls.Add(Me.btn0)
        Me.Controls.Add(Me.btnDot)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btnCross)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btn6)
        Me.Controls.Add(Me.btnMinus)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.btn9)
        Me.Controls.Add(Me.btnPlus)
        Me.Controls.Add(Me.btn8)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.btnC)
        Me.Controls.Add(Me.btnPM)
        Me.Controls.Add(Me.btnCE)
        Me.Controls.Add(Me.btnBS)
        Me.Controls.Add(Me.txtDisplay)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.lblDisplayResult)
        Me.Controls.Add(Me.lblOperator)
        Me.Controls.Add(Me.txtOp2)
        Me.Controls.Add(Me.txtOp1)
        Me.Controls.Add(Me.BtnMolt)
        Me.Controls.Add(Me.BtnDiv)
        Me.Controls.Add(Me.BtnMin)
        Me.Controls.Add(Me.BtnAdd)
        Me.Controls.Add(Me.Result)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Title)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calcolatrice"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Title As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Result As Label
    Friend WithEvents BtnAdd As Button
    Friend WithEvents BtnMin As Button
    Friend WithEvents BtnDiv As Button
    Friend WithEvents BtnMolt As Button
    Friend WithEvents txtOp1 As TextBox
    Friend WithEvents txtOp2 As TextBox
    Friend WithEvents lblOperator As Label
    Friend WithEvents lblDisplayResult As Label
    Friend WithEvents BtnClear As Button
    Friend WithEvents BtnExit As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents txtDisplay As TextBox
    Friend WithEvents btnBS As Button
    Friend WithEvents btnCE As Button
    Friend WithEvents btnC As Button
    Friend WithEvents btnPM As Button
    Friend WithEvents btn9 As Button
    Friend WithEvents btnPlus As Button
    Friend WithEvents btn8 As Button
    Friend WithEvents btn7 As Button
    Friend WithEvents btn6 As Button
    Friend WithEvents btnMinus As Button
    Friend WithEvents btn5 As Button
    Friend WithEvents btn4 As Button
    Friend WithEvents btn3 As Button
    Friend WithEvents btnCross As Button
    Friend WithEvents btn2 As Button
    Friend WithEvents btn1 As Button
    Friend WithEvents btnResult As Button
    Friend WithEvents btnDivide As Button
    Friend WithEvents btn0 As Button
    Friend WithEvents btnDot As Button
    Friend WithEvents LogDisplay As Label
End Class
