﻿Public Class Form1
    Private Sub BtnAdd_Click(sender As Object, e As EventArgs) Handles BtnAdd.Click
        Dim Operand1 As Double
        Dim Operand2 As Double
        Dim Result As Double

        Operand1 = Double.Parse(txtOp1.Text)
        Operand2 = Double.Parse(txtOp2.Text)
        Result = Operand1 + Operand2

        lblOperator.Text = "+"
        lblDisplayResult.Text = Result.ToString("N")
    End Sub

    Private Sub BtnMin_Click(sender As Object, e As EventArgs) Handles BtnMin.Click
        Dim Operand1 As Double
        Dim Operand2 As Double
        Dim Result As Double

        Operand1 = Double.Parse(txtOp1.Text)
        Operand2 = Double.Parse(txtOp2.Text)
        Result = Operand1 - Operand2

        lblOperator.Text = "-"
        lblDisplayResult.Text = Result.ToString("N")
    End Sub

    Private Sub BtnMolt_Click(sender As Object, e As EventArgs) Handles BtnMolt.Click
        Dim Operand1 As Double
        Dim Operand2 As Double
        Dim Result As Double

        Operand1 = Double.Parse(txtOp1.Text)
        Operand2 = Double.Parse(txtOp2.Text)
        Result = Operand1 * Operand2

        lblOperator.Text = "*"
        lblDisplayResult.Text = Result.ToString("N")
    End Sub

    Private Sub BtnDiv_Click(sender As Object, e As EventArgs) Handles BtnDiv.Click
        Dim Operand1 As Double
        Dim Operand2 As Double
        Dim Result As Double

        Operand1 = Double.Parse(txtOp1.Text)
        Operand2 = Double.Parse(txtOp2.Text)
        Result = Operand1 / Operand2

        lblOperator.Text = "/"
        lblDisplayResult.Text = Result.ToString("N")
    End Sub

    Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        txtOp1.Clear()
        txtOp2.Clear()
        lblOperator.Text = ""
        lblDisplayResult.Text = ""

        txtOp1.Focus()
    End Sub

    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Close()
    End Sub

    'Fine calcolatrice semplice'

    Dim n1, n2, r As Double
    Dim op As String
    Dim OpDone As Boolean

    Private Sub btnResult_Click(sender As Object, e As EventArgs) Handles btnResult.Click
        n2 = txtDisplay.Text
        LogDisplay.Text = LogDisplay.Text + " " + txtDisplay.Text
        OpDone = True

        Select Case op
            Case "+"
                r = n1 + n2
                txtDisplay.Text = r
            Case "-"
                r = n1 - n2
                txtDisplay.Text = r
            Case "*"
                r = n1 * n2
                txtDisplay.Text = r
            Case "/"
                r = n1 / n2
                txtDisplay.Text = r
        End Select
    End Sub

    Private Sub btnCE_Click(sender As Object, e As EventArgs) Handles btnCE.Click
        txtDisplay.Clear()

        Dim fn, sn As String

        fn = Convert.ToString(n1)
        sn = Convert.ToString(n2)

        fn = ""
        sn = ""

    End Sub

    Private Sub btnC_Click(sender As Object, e As EventArgs) Handles btnC.Click
        txtDisplay.Text = "0"
        LogDisplay.Text = ""
    End Sub

    Private Sub btnPM_Click(sender As Object, e As EventArgs) Handles btnPM.Click
        Dim pm As Double

        pm = Convert.ToDouble(txtDisplay.Text)
        txtDisplay.Text = Convert.ToString(-1 * pm)
    End Sub

    Private Sub btnBS_Click(sender As Object, e As EventArgs) Handles btnBS.Click
        If (txtDisplay.Text.Length > 0 And Not txtDisplay.Text = "NaN") Then
            txtDisplay.Text = txtDisplay.Text.Remove(txtDisplay.Text.Length - 1, 1)
        End If

        If (txtDisplay.Text = "" Or txtDisplay.Text = "NaN") Then
            txtDisplay.Text = "0"
        End If
    End Sub

    Private Sub NumberClick(sender As Object, e As EventArgs) Handles MyBase.Click, btnDot.Click, btn9.Click, btn8.Click, btn7.Click, btn6.Click, btn5.Click, btn4.Click, btn3.Click, btn2.Click, btn1.Click, btn0.Click
        Dim b As Button = sender

        If OpDone = True Then
            OpDone = False
            txtDisplay.Clear()
            LogDisplay.Text = ""
        End If
        If Not txtDisplay.Text = "NaN" Then
            If (txtDisplay.Text = "0" And Not b.Text = ",") Then
                txtDisplay.Clear()
                txtDisplay.Text = b.Text
            ElseIf (b.Text = ",") Then
                If (Not txtDisplay.Text.Contains(",")) Then
                    txtDisplay.Text = txtDisplay.Text + b.Text
                End If
            ElseIf (b.Text = "0") Then
                If (Not txtDisplay.Text = "0") Then
                    txtDisplay.Text = txtDisplay.Text + b.Text
                End If
            Else
                txtDisplay.Text = txtDisplay.Text + b.Text
            End If
        Else
            If Not b.Text = "," Then
                txtDisplay.Text = b.Text
            End If
        End If



    End Sub

    Private Sub ClicOperator(sender As Object, e As EventArgs) Handles btnPlus.Click, btnMinus.Click, btnDivide.Click, btnCross.Click
        Dim b As Button = sender

        n1 = txtDisplay.Text
        op = b.Text
        LogDisplay.Text = txtDisplay.Text
        txtDisplay.Text = "0"
        LogDisplay.Text = LogDisplay.Text + " " + op

    End Sub
End Class
